#Snipledge
Rapid knowledge management - snippified

## Installation
* Go to your browser
* Add a new bookmark
* Give it a name (recommended: Snipledgify)
* Paste the code from index.js into the url field

## Usage
* Go to a terrific, shiny cat pic website
* Hit the snipledge bookmarklet
* (optional) change the snippet
* Done! It's already saved