javascript:

function getSnipledgeUrl(options) {
	var options = options || {};
	var title = options.title || document.title;
	var content = options.content || window.location.href;
	var url = 'https://f7-snipledge.firebaseapp.com/snipledges/new';
	var params = {};
	
	if (title) {
		params.title = title;
	}
	
	if (content) {
		params.content = content;
	}
	
	var firstParam = true;
	for (var i in params) {
		var param = params[i];
		
		if (firstParam) {
			url += '?';
			firstParam = false;
		} else {
			url += '&';
		}
		
		url += i + '=' + param;
	}
	
	return url;
}

window.open(getSnipledgeUrl(), 'snipledge', 'toolbar=no');